#CORE API
from django.conf.urls import url
from garages.views import GaragesListAPIView

urlpatterns = [
	#URL API configuration for Questions
	url(r'^garages/$', GaragesListAPIView.as_view(), name='garages_list'),
]
