from __future__ import unicode_literals
from django.contrib.auth.models import User
from django.db import models
from django.template import defaultfilters
from userena.models import UserenaBaseProfile

class UserProfile(UserenaBaseProfile):
        REQUIRED_FIELDS = ('user', 'identity_number', 'car_plate')
        user = models.OneToOneField(User, unique=True, related_name='profile')
        identity_number = models.CharField(max_length=30)
        car_plate = models.CharField(max_length=30)
        slug = models.SlugField(max_length=300)

        #def get_person_full_name

        def save(self, *args, **kwargs):
                if not self.pk:
                        self.slug = defaultfilters.slugify(self.user.get_full_name())
                        while True:
                                try:
                                        user_with_slug_exists = UserProfile.objects.get(slug__iexact=self.slug)
                                        if user_with_slug_exists:
                                                self.slug = "{0}-{1}".format(self.slug, sha1(str(random.random()).encode('utf-8')).hexdigest()[:5])
                                except UserProfile.DoesNotExist: break
                super(UserProfile, self).save(*args, **kwargs)
