from django.contrib import admin
from .models import Garage

admin.site.register(Garage)