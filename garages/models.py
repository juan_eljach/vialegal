from django.db import models

CITY_CHOICES = (
	("bucaramanga", "Bucaramanga"),
	("bogota", "Bogota"),
	("medellin", "Medellin"),
	("cali", "Cali"),
	("barranquilla", "Barranquilla")
)

class Garage(models.Model):
	name = models.CharField(max_length=100)
	description = models.TextField(max_length=300)
	city = models.CharField(max_length=300, choices=CITY_CHOICES)
	latitude = models.CharField(max_length=200)
	longitude = models.CharField(max_length=200)

	def __str__(self):
		return self.name
