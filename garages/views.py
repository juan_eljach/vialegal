from django.shortcuts import render
from rest_framework import generics
from .models import Garage
from .serializers import GarageSerializer


class GaragesListAPIView(generics.ListAPIView):
	model_class = Garage
	serializer_class = GarageSerializer

	def get_queryset(self):
		city = self.request.GET.get("city")
		return Garage.objects.filter(city=city)